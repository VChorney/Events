CREATE TABLE t_user(id SERIAL,
username text NOT NULL,
email text NOT NULL,
password text NOT NULL,
role text NOT NULL,primary key (id));

CREATE TABLE tags(
id SERIAL,
title text NOT NULL,
primary key (id));

CREATE TABLE organization(
id SERIAL,
title text NOT NULL,
country text NOT NULL,
sphere text NOT NULL,
primary key (id));

CREATE TABLE events(
id SERIAL,
id_organization integer,
title text NOT NULL,
location text NOT NULL,
datetime_of_begin timestamp without time zone NOT NULL,
datetime_of_the_end timestamp without time zone NOT NULL,
price numeric,
primary key(id),
foreign key(id_organization) references organization(id));

CREATE TABLE users_events(
id_user integer NOT NULL,
id_event integer NOT NULL,
foreign key(id_user) references t_user(id),
foreign key(id_event) references events(id));

CREATE TABLE events_tags(
id_event integer NOT NULL,
id_tag integer NOT NULL,
foreign key(id_tag) references tags(id),
foreign key(id_event)references events(id));

