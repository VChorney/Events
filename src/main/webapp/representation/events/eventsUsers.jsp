<%--
  Created by IntelliJ IDEA.
  User: vchornetc
  Date: 02.07.2019
  Time: 13:34
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<c:out value="${event.title}"></c:out>
<c:out value="${event.organization.title}"></c:out>
<c:out value="${event.location}"></c:out>
<c:out value="${event.dateTimeOfBegin}"></c:out>
<c:out value="${event.dateTimeOfTheEnd}"></c:out>
<c:out value="${event.price}$"></c:out>
<br>
    <c:forEach items="${requestScope.eventWithUsers}" var="eventWithUsers">
        ${eventWithUsers.userName}
    </c:forEach>
</body>
</html>
