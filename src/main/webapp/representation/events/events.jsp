<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Events</title>
</head>
<body>
<table class="table table-striped">
    <tbody>
    <c:forEach items="${requestScope.event}" var="event">
    <tr>
        <td><a href="eventInfo?id=${event.id}">${event.id}</a></td>
        <td>${event.title}</td>
        <td>${event.organization.title}</td>
        <td>${event.location}</td>
        <td>${event.dateTimeOfBegin}</td>
        <td>${event.dateTimeOfTheEnd}</td>
        <td>${event.price}$</td>
        </a>
    </tr>

    </c:forEach>
</table>
<div align="left">
    <div class="inner-form">
        <div class="input-field first-wrap">
            <div class="input-select">
                <form action="${pageContext.request.contextPath}/events" method="get">
                    <p>Search by tags</p>
                    <select name="tagTitle">
                        <option name="TagTitle" id="TagTitle" value="Tag"> Select tag</option>
                        <br>
                        <c:forEach items="${requestScope.tag}" var="tag">
                            <option value="${tag.title}" name="tagTitle" id="tagTitle">${tag.title}</option>
                            <br>
                        </c:forEach>
                    </select>
                    <p>Search by organization</p>
                    <input id="organization" name="organization" type="text"
                           placeholder="Enter organization's title"/><br>
                    <p>Search by title</p>
                    <input id="title" name="title" type="text" placeholder="Enter title"/><br>
                    <p>Search by location</p>
                    <input id="location" name="location" type="text" placeholder="Enter location"/><br>
                    <p>Search by datetime</p>
                    <p>Datetime of begin</p>
<%--                    <input type="datetime-local" id="dateTimeOfBegin" name="dateTimeOfBegin">--%>

                    <input type="date" id="dateOfBegin" name="dateOfBegin">
                    <input type="time" id="timeOfBegin" name="timeOfBegin">
                    <p>Datetime of the end</p>
<%--                    <input type="datetime-local" id="dateTimeTheEnd" name="dateTimeOfTheEnd">--%>
                    <input type="date" id="dateOfTheEnd" name="dateOfTheEnd">
                    <input type="time" id="timeOfTheEnd" name="timeOfTheEnd">
                    <p>Search by price</p>
                    <p>Lowest price</p>
                        <span class="input-group-addon">$</span>
                        <input type="number" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" id="lowestPrice" name="lowestPrice"/>
                    <p>Lowest price</p>
                    <span class="input-group-addon">$</span>
                    <input type="number" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" id="highestPrice" name="highestPrice"/>
                    <p>Search by keyword</p>
                    <input id="keyword" name="keyword" type="text" placeholder="Enter keyword"/><br>
                    <br>
                    <input type="submit" value="Search">
                </form>
            </div>
            <div class="inputError">
                <c:out value="${error}"></c:out>
            </div>
        </div>
    </div>
</div>
</body>
</html>
