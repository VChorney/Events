<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: vchornetc
  Date: 02.07.2019
  Time: 16:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table class="table table-striped">
    <tbody>
    <c:forEach items="${requestScope.event}" var="event">
    <tr>
        <td><a href="eventInfo?id=${event.id}">${event.id}</a></td>
        <td>${event.title}</td>
        <td>${event.organization.title}</td>
        <td>${event.location}</td>
        <td>${event.dateTimeOfBegin}</td>
        <td>${event.dateTimeOfTheEnd}</td>
        <td>${event.price}$</td>
        </a>
    </tr>

    </c:forEach>
</table>
</body>
</html>
