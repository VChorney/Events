package com.events.service;

import com.events.model.Event;
import com.events.model.User;
import com.events.service.BaseService;

import java.util.List;

public interface UserService extends BaseService<User> {
    User getByEmail(String email);

    List<User> getByRole(String role);

    void addEventUserRelationship(Event event, User user);

    void removeEventUserRelationship(Event event, User user);
}
