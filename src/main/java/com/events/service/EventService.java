package com.events.service;

import com.events.model.Event;
import com.events.model.Organization;
import com.events.model.Tag;
import com.events.service.BaseService;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

public interface EventService extends BaseService<Event> {


    List<Event> getByAll(String tagTitle,String organizationTitle,String title, String location, String dateTimeOfBegin,String dateTimeOfTheEnd, String lowestPrice,String highestPrice, String keyword);

    List<Event> getByTag(String tag);

    List<Event> getByKeyword(String keyword);

    List<Event> getByOrganization(String organizationTitle);

    List<Event> getByLocation(String location);

    List<Event> getByDateTimeOfBegin(Timestamp dateTimeOfBegin);

    List<Event> getByDateTimeOfTheEnd(Timestamp dateTimeOfTheEnd);

    List<Event> getEventsInTime(Timestamp dateTimeOfBegin, Timestamp dateTimeOfTheEnd);

    List<Event> getEventsInPrice(BigDecimal lowestPrice, BigDecimal highestPrice);

    Event getEventsUsers(Event event);

    Event getEventsTags(Event event);

    void addTagEventRelationship(Event event, Tag tag);

    void removeTagEventRelationship(Event event, Tag tag);
}
