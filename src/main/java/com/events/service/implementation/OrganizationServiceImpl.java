package com.events.service.implementation;


import com.events.dao.OrganizationDaoInterface;
import com.events.dao.implemetation.OrganizationDao;
import com.events.model.Organization;
import com.events.service.OrganizationService;

import java.util.List;

public class OrganizationServiceImpl implements OrganizationService {

    private OrganizationDaoInterface organizationDao = new OrganizationDao();

    @Override
    public Organization getById(Integer id) {
        return organizationDao.getById(id);
    }

    @Override
    public Organization getByTitle(String title) {
        return organizationDao.getByTitle(title);
    }

    @Override
    public List<Organization> getByCountry(String country) {
        return organizationDao.getByCountry(country);
    }

    @Override
    public List<Organization> getBySphere(String sphere) {
        return organizationDao.getBySphere(sphere);
    }

    @Override
    public List<Organization> getAll() {
        return organizationDao.getAll();
    }

    @Override
    public void save(Organization organization) {
        organizationDao.save(organization);
    }

    @Override
    public void updateById(Organization organization, Integer id) {
        organizationDao.updateById(organization, id);
    }

    @Override
    public void deleteAll() {
        organizationDao.deleteAll();
    }

    @Override
    public void deleteById(Integer id) {
        organizationDao.deleteById(id);
    }

    @Override
    public Organization getOrganizationsEvents(Organization organization) {
        return organizationDao.getOrganizationsEvents(organization);
    }
}
