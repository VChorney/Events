package com.events.service.implementation;

import com.events.dao.EventDaoInterface;
import com.events.dao.implemetation.EventDao;
import com.events.model.Event;
import com.events.model.Organization;
import com.events.model.Tag;
import com.events.service.EventService;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

public class EventServiceImpl implements EventService {

    private EventDaoInterface eventDao = new EventDao();

    @Override
    public Event getById(Integer id) {
        return eventDao.getById(id);
    }

    @Override
    public List<Event> getByAll(String tagTitle, String organizationTitle, String title, String location, String dateTimeOfBegin, String dateTimeOfTheEnd, String lowestPrice, String highestPrice, String keyword) {
        return eventDao.getByAll(tagTitle,organizationTitle,title,location,dateTimeOfBegin,dateTimeOfTheEnd,lowestPrice,highestPrice,keyword);
    }

    @Override
    public Event getByTitle(String title) {
        return eventDao.getByTitle(title);
    }

    @Override
    public List<Event> getByKeyword(String keyword) {
        return eventDao.getByKeyword(keyword);
    }

    @Override
    public List<Event> getByOrganization(String organizationTitle) {
        return eventDao.getByOrganization(organizationTitle);
    }

    @Override
    public List<Event> getByTag(String tag) {
        return eventDao.getByTag(tag);
    }

    @Override
    public List<Event> getByLocation(String location) {
        return eventDao.getByLocation(location);
    }

    @Override
    public List<Event> getByDateTimeOfBegin(Timestamp dateTimeOfBegin) {
        return eventDao.getByDateTimeOfBegin(dateTimeOfBegin);
    }

    @Override
    public List<Event> getByDateTimeOfTheEnd(Timestamp dateTimeOfTheEnd) {
        return eventDao.getByDateTimeOfTheEnd(dateTimeOfTheEnd);
    }

    @Override
    public List<Event> getEventsInTime(Timestamp dateTimeOfBegin, Timestamp dateTimeOfTheEnd) {
        return eventDao.getEventsInTime(dateTimeOfBegin, dateTimeOfTheEnd);
    }

    @Override
    public List<Event> getEventsInPrice(BigDecimal lowestPrice, BigDecimal highestPrice) {
        return eventDao.getEventsInPrice(lowestPrice, highestPrice);
    }

    @Override
    public List<Event> getAll() {
        return eventDao.getAll();
    }

    @Override
    public void save(Event event) {
        eventDao.save(event);
    }

    @Override
    public void updateById(Event event, Integer id) {
        eventDao.updateById(event, id);
    }

    @Override
    public void deleteAll() {
        eventDao.deleteAll();
    }

    @Override
    public void deleteById(Integer id) {
        eventDao.deleteById(id);
    }

    @Override
    public Event getEventsUsers(Event event) {
        return eventDao.getEventsUsers(event);
    }

    @Override
    public Event getEventsTags(Event event) {
        return eventDao.getEventsTags(event);
    }

    @Override
    public void addTagEventRelationship(Event event, Tag tag) {
        eventDao.addTagEventRelationship(event,tag);
    }

    @Override
    public void removeTagEventRelationship(Event event, Tag tag) {
        eventDao.removeTagEventRelationship(event,tag);
    }
}
