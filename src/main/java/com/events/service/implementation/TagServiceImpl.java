package com.events.service.implementation;

import com.events.dao.TagDaoInterface;
import com.events.dao.implemetation.TagDao;
import com.events.model.Tag;
import com.events.service.TagService;

import java.util.List;

public class TagServiceImpl implements TagService {

    private TagDaoInterface tagDao = new TagDao();

    @Override
    public Tag getById(Integer id) {
        return tagDao.getById(id);
    }

    @Override
    public Tag getByTitle(String title) {
        return tagDao.getByTitle(title);
    }

    @Override
    public List<Tag> getAll() {
        return tagDao.getAll();
    }

    @Override
    public void save(Tag tag) {
        tagDao.save(tag);
    }

    @Override
    public void updateById(Tag tag, Integer id) {
        tagDao.updateById(tag, id);
    }

    @Override
    public void deleteAll() {
        tagDao.deleteAll();
    }

    @Override
    public void deleteById(Integer id) {
        tagDao.deleteById(id);
    }

    @Override
    public Tag getEventsTags(Tag tag) {
        return tagDao.getEventsTags(tag);
    }
}
