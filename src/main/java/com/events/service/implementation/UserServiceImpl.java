package com.events.service.implementation;

import com.events.dao.UserDaoInterface;
import com.events.dao.implemetation.UserDao;
import com.events.model.Event;
import com.events.model.User;
import com.events.service.UserService;

import javax.annotation.ManagedBean;
import java.util.List;
@ManagedBean("users")
public class UserServiceImpl implements UserService {

    private UserDaoInterface userDao = new UserDao();

    @Override
    public User getById(Integer id) {
        return userDao.getById(id);
    }

    @Override
    public User getByTitle(String title) {
        return userDao.getByTitle(title);
    }

    @Override
    public User getByEmail(String email) {
        return userDao.getByEmail(email);
    }

    @Override
    public List<User> getByRole(String role) {
        return userDao.getByRole(role);
    }

    @Override
    public List<User> getAll() {
        return userDao.getAll();
    }

    @Override
    public void save(User user) {
        userDao.save(user);
    }

    @Override
    public void updateById(User user, Integer id) {
        userDao.updateById(user, id);
    }

    @Override
    public void deleteAll() {
        userDao.deleteAll();
    }

    @Override
    public void deleteById(Integer id) {
        userDao.deleteById(id);
    }

    @Override
    public void addEventUserRelationship(Event event, User user) {
        userDao.addEventUserRelationship(event,user);
    }

    @Override
    public void removeEventUserRelationship(Event event, User user) {
        userDao.removeEventUserRelationship(event,user);
    }
}
