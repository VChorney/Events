package com.events.service;

import java.util.List;

public interface BaseService<T> {
    T getById(Integer id);

    T getByTitle(String title);

    List<T> getAll();

    void save(T t);

    void updateById(T t, Integer id);

    void deleteAll();

    void deleteById(Integer id);
}
