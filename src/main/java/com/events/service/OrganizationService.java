package com.events.service;

import com.events.model.Organization;
import com.events.service.BaseService;

import java.util.List;

public interface OrganizationService extends BaseService<Organization> {
    List<Organization> getByCountry(String country);

    List<Organization> getBySphere(String sphere);

    Organization getOrganizationsEvents(Organization organization);
}
