package com.events.service;

import com.events.model.Tag;
import com.events.service.BaseService;

public interface TagService extends BaseService<Tag> {
    Tag getEventsTags(Tag tag);
}
