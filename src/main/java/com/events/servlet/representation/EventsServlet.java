package com.events.servlet.representation;

import com.events.model.Event;
import com.events.service.EventService;
import com.events.service.TagService;
import com.events.service.implementation.EventServiceImpl;
import com.events.service.implementation.TagServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

@WebServlet(name = "events", urlPatterns = "/events")
public class EventsServlet extends HttpServlet {

    private EventService event = new EventServiceImpl();
    private TagService tagService = new TagServiceImpl();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("tag", tagService.getAll());
        if ((Objects.equals(request.getQueryString(), "tagTitle=Tag&organization=&title=&location=&dateOfBegin=&timeOfBegin=&dateOfTheEnd=&timeOfTheEnd=&lowestPrice=&highestPrice=&keyword=")) || (request.getQueryString() == null)) {
            request.setAttribute("event", event.getAll());
            request.getRequestDispatcher("representation/events/events.jsp").forward(request, response);
        } else {
            if ((!(formatDateTime((request.getParameter("dateOfBegin")), request.getParameter("timeOfBegin")).compareTo(formatDateTime((request.getParameter("dateOfTheEnd")), request.getParameter("timeOfTheEnd"))) < 0))
                    && ((!formatDateTime((request.getParameter("dateOfBegin")), request.getParameter("timeOfBegin")).equals("")) && (!formatDateTime((request.getParameter("dateOfTheEnd")), request.getParameter("timeOfTheEnd")).equals("")))) {
                request.setAttribute("error", "Datetime of the end mast be bigger then datetime of begin");
                request.getRequestDispatcher("error").forward(request, response);
            } else {
                if (!(request.getParameter("lowestPrice").compareTo(request.getParameter("highestPrice")) < 0)) {
                    request.setAttribute("error", "Highest be bigger then lowest one");
                    request.getRequestDispatcher("error").forward(request, response);
                } else {
                    String query = formatQueryString(request);
                    if (!(((query.contains("=some&")) && (!query.contains("keyword=some"))) || ((!query.contains("=some&")) && (query.contains("keyword=some"))))) {
                        request.setAttribute("error", "blabla");
                        request.getRequestDispatcher("error.jsp").forward(request, response);
                    }
                    request.setAttribute("event", event.getByAll(request.getParameter("tagTitle"), request.getParameter("organization"), request.getParameter("title"),
                            request.getParameter("location"), formatDateTime((request.getParameter("dateOfBegin")), request.getParameter("timeOfBegin")), formatDateTime((request.getParameter("dateOfTheEnd")), request.getParameter("timeOfTheEnd")), request.getParameter("lowestPrice"), request.getParameter("highestPrice"), request.getParameter("keyword")));
                }
            }
            request.getRequestDispatcher("representation/events/events.jsp").forward(request, response);
        }
    }


    private String formatDateTime(String dateQuery, String timeQuery) {
        StringBuilder dateTime = new StringBuilder(dateQuery);
        if (!dateTime.toString().equals("")) {
            if (!Objects.equals(timeQuery, "")) {
                dateTime.append(" ").append(timeQuery).append(":00");
            } else dateTime.append(" 00:00:00");
            return dateTime.toString();
        } else return "";
    }

    private String formatQueryString(HttpServletRequest request) {
        if (request.getQueryString() != null) {
            StringBuilder query = new StringBuilder(request.getQueryString());
            if (!Objects.equals(request.getParameter("tagTitle"), "Tag")) {
                query.replace(query.indexOf("=") + 1, query.indexOf("&") + 1, "some&");
            }
            if (!Objects.equals(request.getParameter("organization"), "")) {
                query.replace(query.indexOf("&organization="), query.indexOf("title="), "&organization=some&");
                System.out.println(query);
            }
            if (!Objects.equals(request.getParameter("title"), "")) {
                query.replace(query.indexOf("&title="), query.indexOf("location="), "&title=some&");
                System.out.println(query);
            }
            if (!Objects.equals(request.getParameter("location"), "")) {
                query.replace(query.indexOf("&location="), query.indexOf("dateOfBegin="), "&location=some&");
                System.out.println(query);
            }
            if (!Objects.equals(request.getParameter("dateOfBegin"), "")) {
                query.replace(query.lastIndexOf("&dateOfBegin="), query.indexOf("timeOfBegin="), "&dateOfBegin=some&");
                System.out.println(query);
            }
            if (!Objects.equals(request.getParameter("timeOfBegin"), "")) {
                query.replace(query.lastIndexOf("&timeOfBegin="), query.indexOf("dateOfTheEnd="), "&timeOfBegin=some&");
                System.out.println(query);
            }
            if (!Objects.equals(request.getParameter("dateOfTheEnd"), "")) {
                query.replace(query.lastIndexOf("&dateOfTheEnd="), query.indexOf("dateOfTheEnd="), "&dateOfTheEnd=some&");
                System.out.println(query);
            }
            if (!Objects.equals(request.getParameter("timeOfTheEnd"), "")) {
                query.replace(query.lastIndexOf("&timeOfTheEnd="), query.indexOf("lowestPrice="), "&timeOfTheEnd=some&");
                System.out.println(query);
            }
            if (!Objects.equals(request.getParameter("lowestPrice"), "")) {
                query.replace(query.lastIndexOf("&lowestPrice="), query.indexOf("highestPrice="), "&lowestPrice=some&");
                System.out.println(query);
            }
            if (!Objects.equals(request.getParameter("highestPrice"), "")) {
                query.replace(query.lastIndexOf("&highestPrice="), query.indexOf("keyword="), "&highestPrice=some&");
                System.out.println(query);
            }
            if (!Objects.equals(request.getParameter("keyword"), "")) {
                query.replace(query.lastIndexOf("&keyword="), query.length(), "&keyword=some");
                System.out.println(query);
            }
            return query.toString();
        } else return null;
    }
}

