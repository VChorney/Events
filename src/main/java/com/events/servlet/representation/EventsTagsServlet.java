package com.events.servlet.representation;

import com.events.service.EventService;
import com.events.service.implementation.EventServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "EventsTagsServlet", urlPatterns = "/eventsTags")
public class EventsTagsServlet extends HttpServlet {
    private EventService eventService = new EventServiceImpl();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("event",eventService.getEventsTags(eventService.getById(Integer.valueOf(request.getParameter("id")))));
        request.setAttribute("eventWithTags",eventService.getEventsUsers(eventService.getById(Integer.valueOf(request.getParameter("id")))).getTags());
        request.getRequestDispatcher("representation/events/eventsTags.jsp").forward(request, response);
    }
}
