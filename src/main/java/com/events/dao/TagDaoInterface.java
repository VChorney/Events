package com.events.dao;

import com.events.model.Tag;

public interface TagDaoInterface extends BaseDao<Tag> {
    Tag getEventsTags(Tag tag);
}
