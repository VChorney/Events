package com.events.dao.implemetation;

import com.events.connection.ConnectionFactory;
import com.events.connectionImplementation.ConnectToEventsCatalog;

import com.events.dao.EventDaoInterface;
import com.events.model.Tag;
import com.events.model.Event;
import com.events.model.User;
import com.events.model.Roles;
import com.events.model.Organization;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

public class EventDao implements EventDaoInterface {

    private ConnectionFactory connection = new ConnectToEventsCatalog();
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    @Override
    public Event getById(Integer id) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT events.id AS id_event, events.title AS " +
                    "events_title, events.location, events.datetime_of_begin,events.datetime_of_the_end,events.price,\n" +
                    "organization.id AS id_organization, organization.title AS organizations_title, organization.country,organization.sphere\n" +
                    "FROM events join organization ON events.id_organization=organization.id WHERE events.id = ?");
            preparedStatement.setInt(1, id);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                return setEvent(result);
            } else
                return null;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    @Override
    public List<Event> getByKeyword(String keyword) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT events.id AS id_event, events.title AS events_title, events.location, events.datetime_of_begin,events.datetime_of_the_end,events.price,\n" +
                    "organization.id AS id_organization, organization.title AS organizations_title, organization.country,organization.sphere\n" +
                    "FROM events join organization ON events.id_organization=organization.id \n" +
                    "WHERE events.title LIKE ? OR events.location LIKE ? \n" +
                    "OR organization.title LIKE ? OR organization.country LIKE ? OR organization.sphere LIKE ?;");
            preparedStatement.setString(1, "%" + keyword + "%");
            preparedStatement.setString(2, "%" + keyword + "%");
            preparedStatement.setString(3, "%" + keyword + "%");
            preparedStatement.setString(4, "%" + keyword + "%");
            preparedStatement.setString(5, "%" + keyword + "%");
            ResultSet result = preparedStatement.executeQuery();
            return getList(result);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    @Override
    public Event getByTitle(String title) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT events.id AS id_event, events.title AS " +
                    "events_title, events.location, events.datetime_of_begin,events.datetime_of_the_end,events.price,\n" +
                    "organization.id AS id_organization, organization.title AS organizations_title, organization.country,organization.sphere\n" +
                    "FROM events join organization ON events.id_organization=organization.id WHERE events.title=?");
            preparedStatement.setString(1, title);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                return setEvent(result);
            } else
                return null;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    @Override
    public List<Event> getByOrganization(String organizationTitle) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT events.id AS id_event, events.title AS events_title, events.location, events.datetime_of_begin,\n" +
                    "events.datetime_of_the_end,events.price,organization.id AS id_organization, organization.title\n" +
                    "AS organizations_title, organization.country,organization.sphere\n" +
                    "FROM events join organization ON events.id_organization=organization.id WHERE organization.title =?");
            preparedStatement.setString(1, organizationTitle);
            ResultSet result = preparedStatement.executeQuery();

            return getList(result);

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    @Override
    public List<Event> getByTag(String tag) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT events.id AS id_event, events.title AS \n" +
                    "                    events_title, events.location, events.datetime_of_begin,events.datetime_of_the_end,events.price,\n" +
                    "                    organization.id AS id_organization, organization.title AS organizations_title, organization.country,organization.sphere\n" +
                    "\tFROM events_tags JOIN events ON events.id=events_tags.id_event JOIN tags ON tags.id=events_tags.id_tag JOIN organization ON organization.id=events.id_organization WHERE tags.title=?");
            preparedStatement.setString(1, tag);
            ResultSet result = preparedStatement.executeQuery();

            return getList(result);

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    @Override
    public List<Event> getByLocation(String location) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT events.id AS id_event, events.title AS " +
                    "events_title, events.location, events.datetime_of_begin,events.datetime_of_the_end,events.price,\n" +
                    "organization.id AS id_organization, organization.title AS organizations_title, organization.country,organization.sphere\n" +
                    "FROM events join organization ON events.id_organization=organization.id WHERE events.location=?");
            preparedStatement.setString(1, location);
            ResultSet result = preparedStatement.executeQuery();
            return getList(result);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    @Override
    public List<Event> getByDateTimeOfBegin(Timestamp dateTimeOfBegin) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT events.id AS id_event, events.title AS " +
                    "events_title, events.location, events.datetime_of_begin,events.datetime_of_the_end,events.price,\n" +
                    "organization.id AS id_organization, organization.title AS organizations_title, organization.country,organization.sphere\n" +
                    "FROM events join organization ON events.id_organization=organization.id WHERE events.datetime_of_begin>=?");
            preparedStatement.setTimestamp(1, dateTimeOfBegin);
            ResultSet result = preparedStatement.executeQuery();

            return getList(result);

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public List<Event> getByDateTimeOfTheEnd(Timestamp dateTimeOfTheEnd) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT events.id AS id_event, events.title AS " +
                    "events_title, events.location, events.datetime_of_begin,events.datetime_of_the_end,events.price,\n" +
                    "organization.id AS id_organization, organization.title AS organizations_title, organization.country,organization.sphere\n" +
                    "FROM events join organization ON events.id_organization=organization.id WHERE events.datetime_of_the_end<=?");
            preparedStatement.setTimestamp(1, dateTimeOfTheEnd);
            ResultSet result = preparedStatement.executeQuery();

            return getList(result);

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    @Override
    public List<Event> getEventsInTime(Timestamp dateTimeOfBegin, Timestamp dateTimeOfTheEnd) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT events.id AS id_event, events.title AS " +
                    "events_title, events.location, events.datetime_of_begin,events.datetime_of_the_end,events.price,\n" +
                    "organization.id AS id_organization, organization.title AS organizations_title, organization.country,organization.sphere\n" +
                    "FROM events join organization ON events.id_organization=organization.id WHERE events.datetime_of_begin>=? AND events.datetime_of_the_end<=?");
            preparedStatement.setTimestamp(1, dateTimeOfBegin);
            preparedStatement.setTimestamp(2, dateTimeOfTheEnd);
            ResultSet result = preparedStatement.executeQuery();

            return getList(result);

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    @Override
    public List<Event> getEventsInPrice(BigDecimal lowestPrice, BigDecimal highestPrice) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT events.id AS id_event, events.title AS " +
                    "events_title, events.location, events.datetime_of_begin,events.datetime_of_the_end,events.price,\n" +
                    "organization.id AS id_organization, organization.title AS organizations_title, organization.country,organization.sphere\n" +
                    "FROM events join organization ON events.id_organization=organization.id WHERE events.price>=? AND events.price<=?");
            preparedStatement.setBigDecimal(1, lowestPrice);
            preparedStatement.setBigDecimal(2, highestPrice);
            ResultSet result = preparedStatement.executeQuery();

            return getList(result);

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    @Override
    public List<Event> getByAll(String tagTitle, String organizationTitle, String title, String location, String dateTimeOfBegin, String dateTimeOfTheEnd, String lowestPrice, String highestPrice, String keyword) {
        try {
            int counter = 0;
            StringBuilder query = new StringBuilder("SELECT events.id AS id_event, events.title AS events_title, events.location, events.datetime_of_begin,\n" +
                    "events.datetime_of_the_end,events.price, organization.id AS id_organization, organization.title AS organizations_title, \n" +
                    "organization.country,organization.sphere FROM events_tags JOIN events ON events.id=events_tags.id_event JOIN tags \n" +
                    "ON tags.id=events_tags.id_tag JOIN organization ON organization.id=events.id_organization WHERE");
//            Map<Integer, Map<String, String>> param = new HashMap<>();
//            Map<String, String> value = new HashMap<>();
            List<ArrayList<String>> param = new ArrayList<>();
            if ((!Objects.equals(tagTitle, "")) && (!Objects.equals(tagTitle, "Tag"))) {
                if ((query.charAt(query.length() - 1) == '?') && (query.charAt(query.length() - 1) != 'E')) {
                    query.append(" AND");
                }
                param.add(new ArrayList<>());
                param.get(counter).add(String.valueOf(counter));
                param.get(counter).add("tagTitle");
                param.get(counter++).add(tagTitle);
                query.append(" tags.title LIKE ?");
            }
            if ((!Objects.equals(organizationTitle, ""))) {
                if ((query.charAt(query.length() - 1) == '?') && (query.charAt(query.length() - 1) != 'E')) {
                    query.append(" AND");
                }
                param.add(new ArrayList<>());
                param.get(counter).add(String.valueOf(counter));
                param.get(counter).add("organizationTitle");
                param.get(counter++).add(organizationTitle);
                query.append(" organization.title LIKE ?");
            }
            if ((!Objects.equals(title, ""))) {
                if ((query.charAt(query.length() - 1) == '?') && (query.charAt(query.length() - 1) != 'E')) {
                    query.append(" AND");
                }
                param.add(new ArrayList<>());
                param.get(counter).add(String.valueOf(counter));
                param.get(counter).add("title");
                param.get(counter++).add(title);
                query.append(" events.title LIKE ?");
            }
            if ((!Objects.equals(location, ""))) {
                if ((query.charAt(query.length() - 1) == '?') && (query.charAt(query.length() - 1) != 'E')) {
                    query.append(" AND");
                }
                param.add(new ArrayList<>());
                param.get(counter).add(String.valueOf(counter));
                param.get(counter).add("location");
                param.get(counter++).add(location);
                query.append(" events.location LIKE ?");
            }
            if ((!Objects.equals(dateTimeOfBegin, ""))) {
                if ((query.charAt(query.length() - 1) == '?') && (query.charAt(query.length() - 1) != 'E')) {
                    query.append(" AND");
                }
                param.add(new ArrayList<>());
                param.get(counter).add(String.valueOf(counter));
                param.get(counter).add("dateTimeOfBegin");
                param.get(counter++).add(dateTimeOfBegin);
                query.append(" events.datetime_of_begin>=?");
            }
            if ((!Objects.equals(dateTimeOfTheEnd, ""))) {
                if ((query.charAt(query.length() - 1) == '?') && (query.charAt(query.length() - 1) != 'E')) {
                    query.append(" AND");
                }
                param.add(new ArrayList<>());
                param.get(counter).add(String.valueOf(counter));
                param.get(counter).add("dateTimeOfTheEnd");
                param.get(counter++).add(dateTimeOfTheEnd);
                query.append(" events.datetime_of_the_end<=?");
            }
            if ((!Objects.equals(lowestPrice, ""))) {
                if ((query.charAt(query.length() - 1) == '?') && (query.charAt(query.length() - 1) != 'E')) {
                    query.append(" AND");
                }
                param.add(new ArrayList<>());
                param.get(counter).add(String.valueOf(counter));
                param.get(counter).add("lowestPrice");
                param.get(counter++).add(lowestPrice);
                query.append(" events.price>=?");
            }
            if ((!Objects.equals(highestPrice, ""))) {
                if ((query.charAt(query.length() - 1) == '?') && (query.charAt(query.length() - 1) != 'E')) {
                    query.append(" AND");
                }
                param.add(new ArrayList<>());
                param.get(counter).add(String.valueOf(counter));
                param.get(counter).add("highestPrice");
                param.get(counter++).add(highestPrice);
                query.append(" events.price<=?");
            }
            if ((!Objects.equals(keyword, ""))) {
                if ((query.charAt(query.length() - 1) == '?') && (query.charAt(query.length() - 1) != 'E')) {
                    query.append(" AND");
                }
                param.add(new ArrayList<>());
                param.get(counter).add(String.valueOf(counter));
                param.get(counter).add("keyword");
                param.get(counter).add(keyword);
                query.append(" tags.title LIKE ? OR organization.title LIKE ? OR events.title LIKE ? OR events.location LIKE ?");
            }
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement(query.toString());
            for (ArrayList params : param) {
                if ((params.get(1).equals("tagTitle"))) {
                    preparedStatement.setString(Integer.valueOf(params.get(0).toString())+1, "%" + params.get(2)+ "%");
                    continue;
                }
                if ((params.get(1).equals("organizationTitle"))) {
                    preparedStatement.setString(Integer.valueOf(params.get(0).toString())+1, "%" + params.get(2)+ "%");
                    continue;
                }

                if ((params.get(1).equals("title"))) {
                    preparedStatement.setString(Integer.valueOf(params.get(0).toString())+1, "%" + params.get(2)+ "%");
                    continue;
                }
                if ((params.get(1).equals("location"))) {
                    preparedStatement.setString(Integer.valueOf(params.get(0).toString())+1, "%" + params.get(2)+ "%");
                    continue;
                }
                if ((params.get(1).equals("dateTimeOfBegin"))) {
                    preparedStatement.setTimestamp(Integer.valueOf(params.get(0).toString())+1, Timestamp.valueOf(params.get(2).toString()));
                    continue;
                }
                if ((params.get(1).equals("dateTimeOfTheEnd"))) {
                    preparedStatement.setTimestamp(Integer.valueOf(params.get(0).toString())+1, Timestamp.valueOf(params.get(2).toString()));
                    continue;
                }
                if ((params.get(1).equals("lowestPrice"))) {
                    preparedStatement.setBigDecimal(Integer.valueOf(params.get(0).toString())+1, new BigDecimal(params.get(2).toString()));
                    continue;
                }
                if ((params.get(1).equals("highestPrice"))) {
                    preparedStatement.setBigDecimal(Integer.valueOf(params.get(0).toString())+1, new BigDecimal(params.get(2).toString()));
                    continue;
                }

                if (params.get(1).equals("keyword")) {
                    for (int key = 1; key < 5; key++) {
                        preparedStatement.setString(key, "%" + params.get(2) + "%");
                    }
                }
            }
            ResultSet result = preparedStatement.executeQuery();

            return getList(result);

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    @Override
    public List<Event> getAll() {
        try {
            ResultSet result = connection.getConnection().prepareCall("SELECT events.id AS id_event, events.title AS events_title, events.location,\n" +
                    "events.datetime_of_begin,events.datetime_of_the_end,events.price, \n" +
                    "organization.id AS id_organization, organization.title AS organizations_title, organization.country,organization.sphere\n" +
                    "FROM events join organization ON events.id_organization=organization.id;").executeQuery();

            return getList(result);

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public void save(Event event) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("INSERT INTO events(id_organization,title,location,datetime_of_begin,datetime_of_the_end,price) VALUES (?,?,?,?,?,?)");
            addEventIntoDB(event, preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    private void addEventIntoDB(Event event, PreparedStatement preparedStatement) {
        try {
            preparedStatement.setInt(1, event.getOrganization().getId());
            preparedStatement.setString(2, event.getTitle());
            preparedStatement.setString(3, event.getLocation());
            preparedStatement.setTimestamp(4, Timestamp.valueOf(event.getDateTimeOfBegin()));
            preparedStatement.setTimestamp(5, Timestamp.valueOf(event.getDateTimeOfTheEnd()));
            preparedStatement.setBigDecimal(6, event.getPrice());
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public void updateById(Event event, Integer id) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("UPDATE  events SET id_organization=?,title=?,location=?,datetime_of_begin=?,datetime_of_the_end=?,price=?  WHERE event.id_event=?");
            addEventIntoDB(event, preparedStatement);
            preparedStatement.setInt(7, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public void deleteAll() {
        try {
            connection.getConnection().prepareStatement("DELETE FROM events").executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public void deleteById(Integer id) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("DELETE FROM events WHERE event.id_event=?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public Event getEventsUsers(Event event) {
        List<User> users = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT t_user.* \n" +
                    "FROM t_user join users_events on t_user.id=users_events.id_user join \n" +
                    "events on events.id=users_events.id_event  WHERE events.id=?");
            preparedStatement.setInt(1, event.getId());
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                User user = new User(result.getInt("id"), result.getString("user_name"),
                        result.getString("email"), result.getString("password"), Roles.valueOf(result.getString("role")));
                users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
        event.setUsers(users);
        return event;
    }

    @Override
    public Event getEventsTags(Event event) {
        List<Tag> tags = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT tags.* \n" +
                    "FROM tags join events_tags on tags.id=events_tags.id_tag join \n" +
                    "events on events.id=events_tags.id_event WHERE events.id=?");
            preparedStatement.setInt(1, event.getId());
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                Tag tag = new Tag(result.getInt("id"), result.getString("title"));
                tags.add(tag);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
        event.setTags(tags);
        return event;
    }

    private List<Event> getList(ResultSet result) throws SQLException {
        List<Event> events = new ArrayList<>();
        while (result.next()) {
            events.add(setEvent(result));
        }
        return events;
    }

    private Event setEvent(ResultSet result) throws SQLException {
        Organization organization = new Organization(result.getInt("id_organization"), result.getString("organizations_title"), result.getString("country"),
                result.getString("sphere"));
        return new Event(result.getInt("id_event"), organization, result.getString("events_title"),
                result.getString("location"), simpleDateFormat.format(result.getTimestamp("datetime_of_begin")), simpleDateFormat.format(result.getTimestamp("datetime_of_the_end")), result.getBigDecimal("price"));
    }

    @Override
    public void addTagEventRelationship(Event event, Tag tag) {
        try {
            ResultSet resultTags = connection.getConnection().prepareStatement("SELECT * FROM tags WHERE tags.title=" + tag.getTitle()).executeQuery();
            PreparedStatement preparedStatement;
            if (!resultTags.next()) {
                TagDao tagDao = new TagDao();
                tagDao.save(tag);
            }
            preparedStatement = connection.getConnection().prepareStatement("SELECT events.id AS id_event, events.title AS\n" +
                    "events_title, events.location, events.datetime_of_begin,events.datetime_of_the_end,events.price,\n" +
                    "tags.id AS id_tag, tags.title AS tags_title\n" +
                    "FROM events, tags WHERE events.title=? AND tags.title=?");
            request(event, tag, preparedStatement, "INSERT INTO events_tags(id_event,id_tag) VALUES (?,?)");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public void removeTagEventRelationship(Event event, Tag tag) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT events.id AS id_event, events.title AS\n" +
                    "events_title, events.location, events.datetime_of_begin,events.datetime_of_the_end,events.price,\n" +
                    "tags.id AS id_tag, tags.title AS tags_title\n" +
                    "FROM events, tags WHERE events.title=? AND tags.title=?");
            request(event, tag, preparedStatement, "DELETE events_tags WHERE id_event=? AND id_tag=?");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    private void request(Event event, Tag tag, PreparedStatement preparedStatement, String s) throws SQLException {
        preparedStatement.setString(1, event.getTitle());
        preparedStatement.setString(2, tag.getTitle());
        ResultSet result = preparedStatement.executeQuery();
        preparedStatement = connection.getConnection().prepareStatement(s);
        preparedStatement.setInt(1, result.getInt("id_event"));
        preparedStatement.setInt(2, result.getInt("id_tag"));
        preparedStatement.executeUpdate();
    }

}
