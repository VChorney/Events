package com.events.dao.implemetation;

import com.events.connection.ConnectionFactory;
import com.events.connectionImplementation.ConnectToEventsCatalog;
import com.events.dao.UserDaoInterface;
import com.events.model.Event;
import com.events.model.Organization;
import com.events.model.User;
import com.events.model.Roles;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class UserDao implements UserDaoInterface {

    private ConnectionFactory connection = new ConnectToEventsCatalog();


    @Override
    public User getById(Integer id) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT * FROM t_user " +
                    " WHERE t_user.id_user=?");
            preparedStatement.setInt(1, id);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                return new User(result.getInt("id"), result.getString("user_name"), result.getString("email"),
                        result.getString("password"), Roles.valueOf(result.getString("role")));
            } else
                return null;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public User getByTitle(String title) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT * FROM t_user " +
                    " WHERE t_user.user_name=?");
            preparedStatement.setString(1, title);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                return new User(result.getInt("id"), result.getString("user_name"), result.getString("email"),
                        result.getString("password"), Roles.valueOf(result.getString("role")));
            } else
                return null;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    public User getByEmail(String email) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT * FROM t_user " +
                    " WHERE t_user.email=?");
            preparedStatement.setString(1, email);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                return new User(result.getInt("id_user"), result.getString("user_name"), result.getString("email"),
                        result.getString("password"), Roles.valueOf(result.getString("role")));
            } else
                return null;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    public List<User> getByRole(String role) {
        try {
            ResultSet result = connection.getConnection().prepareCall("SELECT * FROM public.t_user WHERE t_user.role=?").executeQuery();
            if (result.next()) {
                return getUser(result);
            } else
                return null;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public List<User> getAll() {
        try {
            ResultSet result = connection.getConnection().prepareCall("SELECT * FROM public.t_user").executeQuery();
            if (result.next()) {
                return getUser(result);
            } else
                return null;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public void save(User user) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("INSERT INTO t_user(user_name,email,password,role) VALUES (?,?,?,?)");
            setUser(user, preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public void updateById(User user, Integer id) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("UPDATE t_user SET t_user.user_name=?,t_user.email=?,t_user.password=?,t_user.role=? WHERE t_user.id_user=?");
            setUser(user, preparedStatement);
            preparedStatement.setInt(5, user.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public void deleteAll() {
        try {
            connection.getConnection().prepareStatement("DELETE FROM t_user").executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public void deleteById(Integer id) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("DELETE FROM t_user WHERE t_user.id_user=?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    private List<Event> getUsersEvents(Integer id) throws SQLException {
        List<Event> events = new ArrayList<>();
        PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT events.id AS id_event, events.title AS\n" +
                "events_title, events.location, events.datetime_of_begin,events.datetime_of_the_end,events.price,\n" +
                "organization.id AS id_organization, organization.title AS organizations_title, organization.country,organization.sphere\n" +
                "FROM t_user join users_events on t_user.id=users_events.id_user join events join organization ON \n" +
                "events.id_organization=organization.id on events.id=users_events.id_event WHERE t_user.id=?;");
        preparedStatement.setInt(1, id);
        listEvens(events, preparedStatement);
        return events;
    }

    static void listEvens(List<Event> events, PreparedStatement preparedStatement) throws SQLException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        ResultSet result = preparedStatement.executeQuery();
        while (result.next()) {
            Organization organization = new Organization(result.getInt("id_organization"), result.getString("organizations_title"), result.getString("country"),
                    result.getString("sphere"));
            Event event = new Event(result.getInt("id_event"), organization, result.getString("events_title"),
                    result.getString("location"), simpleDateFormat.format(result.getTimestamp("datedatetime_of_begin")), simpleDateFormat.format(result.getTimestamp("datetime_of_the_end")), result.getBigDecimal("price"));
            events.add(event);
        }
    }

    private void setUser(User user, PreparedStatement preparedStatement) {
        try {
            preparedStatement.setString(1, user.getUserName());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getRole().name());
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    private List<User> getUser(ResultSet result) throws SQLException {
        List<User> users = new ArrayList<>();
        while (result.next()) {
            User user = new User(result.getInt("id"), result.getString("user_name"),
                    result.getString("email"), result.getString("password"), Roles.valueOf(result.getString("role")));
            user.setEvents(getUsersEvents(user.getId()));
            users.add(user);
        }
        return users;
    }

    @Override
    public void addEventUserRelationship(Event event, User user) {
        try {
            request(event, user, "INSERT INTO users_events(id_user,id_event) VALUES (?,?)");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }
    @Override
    public void removeEventUserRelationship(Event event, User user) {
        try {
            request(event, user, "DELETE users_events WHERE id_user=? AND id_event=?");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    private void request(Event event, User user, String s) throws SQLException {
        PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT t_user.id AS id_user, events.id AS id_event \n" +
                "FROM t_user join users_events on t_user.id_user=users_events.id_user join \n" +
                "events on events.id_event=users_events.id_event  WHERE events.id_event=? AND t_user.id=?");
        preparedStatement.setInt(1, event.getId());
        preparedStatement.setInt(2, user.getId());
        ResultSet result = preparedStatement.executeQuery();
        preparedStatement = connection.getConnection().prepareStatement(s);
        preparedStatement.setInt(1, result.getInt("id_user"));
        preparedStatement.setInt(2, result.getInt("id_event"));
        preparedStatement.executeUpdate();
    }
}
