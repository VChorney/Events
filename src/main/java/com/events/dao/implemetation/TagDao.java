package com.events.dao.implemetation;

import com.events.connection.ConnectionFactory;
import com.events.connectionImplementation.ConnectToEventsCatalog;
import com.events.dao.TagDaoInterface;
import com.events.model.Event;
import com.events.model.Tag;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TagDao implements TagDaoInterface {
    private ConnectionFactory connection = new ConnectToEventsCatalog();

    @Override
    public Tag getById(Integer id) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT * FROM tags WHERE tags.id=?");
            preparedStatement.setInt(1, id);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                return new Tag(result.getInt("id"), result.getString("title"));
            } else
                return null;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public Tag getByTitle(String title) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT * FROM tags WHERE tags.title=?");
            preparedStatement.setString(1, title);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                return new Tag(result.getInt("id"), result.getString("title"));
            } else
                return null;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public List<Tag> getAll() {
        List<Tag> tags = new ArrayList<>();
        try {
            ResultSet result = connection.getConnection().prepareStatement("SELECT * FROM tags").executeQuery();
            while (result.next()) {
                tags.add(new Tag(result.getInt("id"), result.getString("title")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
        return tags;
    }

    @Override
    public void save(Tag tag) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("INSERT INTO tags(title) VALUES (?)");
            preparedStatement.setString(1, tag.getTitle());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public void updateById(Tag tag, Integer id) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("UPDATE tags SET tags.title=? WHERE tags.id=?");
            preparedStatement.setString(1, tag.getTitle());
            preparedStatement.setInt(2, tag.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public void deleteAll() {
        try {
            connection.getConnection().prepareStatement("DELETE FROM tags").executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public void deleteById(Integer id) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("DELETE FROM tag WHERE tag.id=?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public Tag getEventsTags(Tag tag) {
        List<Event> events = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT events.id AS id_event, events.title AS events_title, events.location, events.datetime_of_begin,\n" +
                    "events.datetime_of_the_end,events.price, organization.id AS id_organization, organization.title AS organizations_title, \n" +
                    "organization.country,organization,sphere\n" +
                    "FROM tags join events_tags on tags.id=events_tags.id_tag join \n" +
                    "events on events.id=events_tags.id_event join organization on organization.id=events.id_organization WHERE events.id=?;");
            preparedStatement.setInt(1, tag.getId());
            UserDao.listEvens(events, preparedStatement);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
        tag.setEvents(events);
        return tag;
    }
}
