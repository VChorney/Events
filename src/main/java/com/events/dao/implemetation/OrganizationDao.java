package com.events.dao.implemetation;

import com.events.connection.ConnectionFactory;
import com.events.connectionImplementation.ConnectToEventsCatalog;
import com.events.dao.OrganizationDaoInterface;
import com.events.model.Event;
import com.events.model.Organization;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class OrganizationDao implements OrganizationDaoInterface {
    private ConnectionFactory connection = new ConnectToEventsCatalog();
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    @Override
    public Organization getById(Integer id) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT * FROM organization WHERE organization.id=?");
            preparedStatement.setInt(1, id);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                return new Organization(result.getInt("id"), result.getString("title"), result.getString("country"),
                        result.getString("sphere"));
            } else
                return null;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public Organization getByTitle(String title) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT * FROM organization WHERE organization.title=?");
            preparedStatement.setString(1, title);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                return new Organization(result.getInt("id"), result.getString("title"), result.getString("country"),
                        result.getString("sphere"));
            } else
                return null;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    public List<Organization> getByCountry(String country) {
        List<Organization> organizations = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT * FROM organization WHERE organization.country=?");
            preparedStatement.setString(1, country);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                organizations.add(new Organization(result.getInt("id"), result.getString("title"), result.getString("country"),
                        result.getString("sphere")));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
        return organizations;
    }

    public List<Organization> getBySphere(String sphere) {
        List<Organization> organizations = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT * FROM organization WHERE organization.sphere=?");
            preparedStatement.setString(1, sphere);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                organizations.add(new Organization(result.getInt("id"), result.getString("title"), result.getString("country"),
                        result.getString("sphere")));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
        return organizations;
    }

    @Override
    public List<Organization> getAll() {
        List<Organization> organizations = new ArrayList<>();
        try {
            ResultSet result = connection.getConnection().prepareStatement("SELECT * FROM organization").executeQuery();
            while (result.next()) {
                organizations.add(new Organization(result.getInt("id"), result.getString("title"), result.getString("country"),
                        result.getString("sphere")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
        return organizations;
    }

    @Override
    public void save(Organization organization) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("INSERT INTO organization(title, country, sphere) VALUES (?,?,?)");
            addOrganizationIntoDB(organization, preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public void updateById(Organization organization, Integer id) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("UPDATE organization SET organization.title=?,organization.country=?,organization.sphere=?, WHERE organization.id=?");
            addOrganizationIntoDB(organization, preparedStatement);
            preparedStatement.setInt(4, organization.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public void deleteAll() {
        try {
            connection.getConnection().prepareStatement("DELETE FROM organization").executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public void deleteById(Integer id) {
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("DELETE FROM organization WHERE organization.id=?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }
    @Override
    public Organization getOrganizationsEvents(Organization organization) {
        List<Event> events = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.getConnection().prepareStatement("SELECT events.*\n" +
                    "FROM events join organization on events.id_organization=organization.id WHERE organization.id=?");
            preparedStatement.setInt(1, organization.getId());
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                Event event = new Event(result.getInt("id"), organization, result.getString("title"),
                        result.getString("location"), simpleDateFormat.format(result.getTimestamp("datetime_of_begin")), simpleDateFormat.format(result.getTimestamp("datetime_of_the_end")), result.getBigDecimal("price"));
                events.add(event);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
        organization.setEvents(events);
        return organization;
    }

    private void addOrganizationIntoDB(Organization organization, PreparedStatement preparedStatement) {
        try {
            preparedStatement.setString(1, organization.getTitle());
            preparedStatement.setString(2, organization.getCountry());
            preparedStatement.setString(3, organization.getSphere());
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }
}
