package com.events.dao;

import com.events.model.Organization;

import java.util.List;

public interface OrganizationDaoInterface extends BaseDao<Organization> {
    List<Organization> getByCountry(String country);

    List<Organization> getBySphere(String sphere);

    Organization getOrganizationsEvents(Organization organization);
}
