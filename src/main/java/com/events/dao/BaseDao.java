package com.events.dao;

import java.util.List;

public interface BaseDao<T> {
    T getById(Integer id);

    T getByTitle(String title);

    List<T> getAll();

    void save(T t);

    void updateById(T t, Integer id);

    void deleteAll();

    void deleteById(Integer id);

}
