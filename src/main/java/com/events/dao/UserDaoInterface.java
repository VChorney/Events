package com.events.dao;

import com.events.model.Event;
import com.events.model.User;

import java.util.List;

public interface UserDaoInterface extends BaseDao<User> {

    User getByEmail(String email);

    List<User> getByRole(String role);

    void addEventUserRelationship(Event event, User user);

    void removeEventUserRelationship(Event event, User user);

}
