package com.events.connectionImplementation;

import com.events.connection.ConnectionFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectToEventsCatalog implements ConnectionFactory {
    private static final String URL = "jdbc:postgresql://127.0.0.1:5432/event_catalog";
    private static final String USER = "myuser";
    private static final String PASS = "password";

    @Override
    public Connection getConnection() {
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(URL, USER, PASS);
        } catch (ClassNotFoundException exeption) {
            throw new RuntimeException("Class not found", exeption.getCause());
        } catch (SQLException e) {
            throw new RuntimeException("Error connecting to the database", e.getCause());
        }
    }

}
