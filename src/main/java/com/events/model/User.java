package com.events.model;

import com.events.service.UserService;
import com.events.service.implementation.UserServiceImpl;
import lombok.Data;

import javax.validation.constraints.*;
import java.util.List;

@Data
public class User {
    private Integer id;
    private List<Event> events;
    @NotEmpty(message = "Username can`t be empty")
    @Size(min = 2, message = "Username can`t be shorter then 2 characters")
    private String userName;
    @NotEmpty(message = "Email can not be empty")
    @Email
    @Size(min = 8, message = "Email can`t be shorter then 8 characters")
    @Pattern(regexp = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$")
    private String email;
    @NotEmpty(message = "Password can`t be empty")
    @Size(min = 8, message = "Password can`t be shorter then 8 characters")
    private String password;
    @NotNull(message = "Role can`t be empty")
    private Roles role;

    public User(Integer id, @NotEmpty(message = "Username can`t be empty") @Size(min = 2, message = "Username can`t be shorter then 2 characters") String userName,
                @NotEmpty(message = "Email can not be empty") @Email @Size(min = 8, message = "Email can`t be shorter then 8 characters") String email,
                @NotEmpty(message = "Password can`t be empty") @Size(min = 8, message = "Password can`t be shorter then 8 characters") String password,
                @NotNull(message = "Role can`t be empty") Roles role) {
        this.id = id;
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public User(@NotEmpty(message = "Username can`t be empty") @Size(min = 2, message = "Username can`t be shorter then 2 characters") String userName,
                @NotEmpty(message = "Email can not be empty") @Email @Size(min = 8, message = "Email can`t be shorter then 8 characters") String email,
                @NotEmpty(message = "Password can`t be empty") @Size(min = 8, message = "Password can`t be shorter then 8 characters") String password,
                @NotNull(message = "Role can`t be empty") Roles role) {
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public User() {
    }

    public boolean uniqueEmail(String email) {
        UserService service = new UserServiceImpl();
        return service.getByEmail(email)!=null;
    }
    public boolean uniqueUserName(String title) {
        UserService service = new UserServiceImpl();
        return service.getByTitle(title)!=null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Roles getRole() {
        return role;
    }

    public void setRole(Roles role) {
        this.role = role;
    }
}
