package com.events.model;

import com.events.service.implementation.EventServiceImpl;
import com.events.service.EventService;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Data
public class Event {
    private Integer id;
    private Organization organization;
    private List<User> users;
    private List<Tag> tags;
    @NotEmpty(message = "  Title can`t be empty")
    @Size(min = 2, message = "  Title can`t be shorter then 2 characters")
    private String title;
    @NotEmpty(message = "Location can`t be empty")
    @Size(min = 2, message = "Location can`t be shorter then 2 characters")
    private String location;
    @NotNull(message = "Time of begin can`t be empty")
    @Size(min = 18, message = "Time of begin can`t be shorter then 18 characters")
    private String dateTimeOfBegin;
    @NotNull(message = "Time of the end can`t be empty")
    @Size(min = 18, message = "Time of begin can`t be shorter then 18 characters")
    private String dateTimeOfTheEnd;
    private BigDecimal price;

    public Event(Integer id, Organization organization, @NotEmpty(message = "  Title can`t be empty") @Size(min = 2, message = "  Title can`t be shorter then 2 characters") String title,
                 @NotEmpty(message = "Location can`t be empty") @Size(min = 2, message = "Location can`t be shorter then 2 characters") String location,
                 @NotNull(message = "Time of begin can`t be empty") @Size(min = 18, message = "Time of begin can`t be shorter then 18 characters") String dateTimeOfBegin,
                 @NotNull(message = "Time of the end can`t be empty") @Size(min = 18, message = "Time of begin can`t be shorter then 18 characters") String dateTimeOfTheEnd, BigDecimal price) {
        this.id = id;
        this.organization = organization;
        this.title = title;
        this.location = location;
        this.dateTimeOfBegin = dateTimeOfBegin;
        this.dateTimeOfTheEnd = dateTimeOfTheEnd;
        this.price = price;
    }

    public Event(Organization organization, @NotEmpty(message = "  Title can`t be empty") @Size(min = 2, message = "  Title can`t be shorter then 2 characters") String title,
                 @NotEmpty(message = "Location can`t be empty") @Size(min = 2, message = "Location can`t be shorter then 2 characters") String location,
                 @NotNull(message = "Time of begin can`t be empty") @Size(min = 18, message = "Time of begin can`t be shorter then 18 characters") String dateTimeOfBegin,
                 @NotNull(message = "Time of the end can`t be empty") @Size(min = 18, message = "Time of begin can`t be shorter then 18 characters") String dateTimeOfTheEnd, BigDecimal price) {
        this.organization = organization;
        this.title = title;
        this.location = location;
        this.dateTimeOfBegin = dateTimeOfBegin;
        this.dateTimeOfTheEnd = dateTimeOfTheEnd;
        this.price = price;
    }

    public Event(Event event) {
        this.id = event.id;
        this.organization = event.organization;
        this.title = event.title;
        this.location = event.location;
        this.dateTimeOfBegin = event.dateTimeOfBegin;
        this.dateTimeOfTheEnd = event.dateTimeOfTheEnd;
        this.price = event.price;
        this.tags=event.tags;
        this.users=event.users;
    }

    public Event() {
    }

    public boolean validateTitle(String title) {
        EventService service = new EventServiceImpl();
        return service.getByTitle(title) != null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDateTimeOfBegin() {
        return dateTimeOfBegin;
    }

    public void setDateTimeOfBegin(String dateTimeOfBegin) {
        this.dateTimeOfBegin = dateTimeOfBegin;
    }

    public String getDateTimeOfTheEnd() {
        return dateTimeOfTheEnd;
    }

    public void setDateTimeOfTheEnd(String dateTimeOfTheEnd) {
        this.dateTimeOfTheEnd = dateTimeOfTheEnd;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
