package com.events.model;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class Tag {
    private Integer id;
    @NotEmpty(message = "Title can`t be empty")
    @Size(min = 2, message = "Title can`t be shorter then 2 characters")
    private String title;
    private List<Event> events;

    public Tag(Integer id, @NotEmpty(message = "Title can`t be empty") @Size(min = 2, message = "Title can`t be shorter then 2 characters") String title) {
        this.id = id;
        this.title = title;
    }

    public Tag(@NotEmpty(message = "Title can`t be empty") @Size(min = 2, message = "Title can`t be shorter then 2 characters") String title) {
        this.title = title;
    }

    public Tag() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }
}
