package com.events.model;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class Organization {

    private Integer id;
    @NotEmpty(message = "Title can`t be empty")
    @Size(min = 2, message = "Title can`t be shorter then 2 characters")
    private String title;
    @NotEmpty(message = "Country can`t be empty")
    @Size(min = 2, message = "Country can`t be shorter then 2 characters")
    private String country;
    @NotEmpty(message = "Sphere can`t be empty")
    @Size(min = 2, message = "Sphere can`t be shorter then 2 characters")
    private String sphere;
    private List<Event> events;

    public Organization(Integer id, @NotEmpty(message = "Title can`t be empty") @Size(min = 2, message = "Title can`t be shorter then 2 characters") String title,
                        @NotEmpty(message = "Country can`t be empty") @Size(min = 2, message = "Country can`t be shorter then 2 characters") String country,
                        @NotEmpty(message = "Sphere can`t be empty") @Size(min = 2, message = "Sphere can`t be shorter then 2 characters") String sphere) {
        this.id = id;
        this.title = title;
        this.country = country;
        this.sphere = sphere;
    }

    public Organization(@NotEmpty(message = "Title can`t be empty") @Size(min = 2, message = "Title can`t be shorter then 2 characters") String title,
                        @NotEmpty(message = "Country can`t be empty") @Size(min = 2, message = "Country can`t be shorter then 2 characters") String country,
                        @NotEmpty(message = "Sphere can`t be empty") @Size(min = 2, message = "Sphere can`t be shorter then 2 characters") String sphere) {
        this.title = title;
        this.country = country;
        this.sphere = sphere;
    }

    public Organization() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSphere() {
        return sphere;
    }

    public void setSphere(String sphere) {
        this.sphere = sphere;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }
}
